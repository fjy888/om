package com.zero2oneit.mall.search.api;

import com.zero2oneit.mall.common.utils.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/2
 */
@RestController
@RequestMapping("/api/search/goods")
public class SearchApi {

    /**
     * 查询商品列表
     * @return
     */
    @PostMapping("/list")
    public R list(){
        return R.ok("操作成功");
    }

}
