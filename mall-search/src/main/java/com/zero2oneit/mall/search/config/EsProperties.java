package com.zero2oneit.mall.search.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "es.client")
@Component
public class EsProperties {

    private String host;// 主机IP

    private String username;// 登录账号

	private String password; //登录密码

    private Integer port;// 端口

}
