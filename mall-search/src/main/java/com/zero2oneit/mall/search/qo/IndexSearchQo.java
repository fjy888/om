package com.zero2oneit.mall.search.qo;

import com.zero2oneit.mall.common.utils.query.QueryObject;
import lombok.Data;

/**
 * @author Lee
 * @date 2021/3/17 -10:03
 */
@Data
public class IndexSearchQo extends QueryObject {

}
