package com.zero2oneit.mall.search.config;


import com.zero2oneit.mall.search.qo.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/2
 */
@SpringBootTest
public class ESConfigTest {

    //注入ElasticsearchRestTemplate
    @Autowired
    private ElasticsearchRestTemplate template;

    //创建索引并增加映射配置
    @Test
    public void createIndex(){
        //创建索引，系统初始化会自动创建索引
        System.out.println("创建索引");
    }

    //删除索引
    @Test
    public void deleteIndex(){
        //创建索引，系统初始化会自动创建索引
        boolean flg = template.deleteIndex(Product.class);
        System.out.println("删除索引 = " + flg);
    }

}