import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/**
 * context : 上下文 ---> 可以获取state中的参数
 * 如：context.state.token
 */
const store = new Vuex.Store({
	state: {
	  token: 'sss',
	  userInfo: uni.getStorageSync('userInfo')
	},
	mutations: {
		// 登录成功，把userinfo存到仓库
	  getUserInfo(state,params){
		  state.userInfo = params
		  uni.setStorageSync('userInfo',params)
	  },
	  // 退出登录，把userinfo存到仓库
	  removeUserInfo(state){
		  state.userInfo = ''
		  uni.removeStorage({key:'userInfo'})
	  }
	},
	actions: {
		getCity(context){
			
		}
	},
	getters:{

	}
})

export default store